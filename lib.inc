section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель rdi на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    
    jmp .loop
    
    .content_loop:
        inc rax
    .loop:
        cmp byte [rdi+rax], 0
        jne .content_loop
    
    ret

; Принимает указатель rdi на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi ; string pointer
    mov rdx, rax ; strlen
    mov rax, 1 ; write
    mov rdi, 1 ; stdout

    syscall

    ret

; Принимает код символа rdi и выводит его в stdout
print_char:
    push rdi

    mov rax, 1 ; write
    mov rsi, rsp ; string pointer
    mov rdi, 1 ; stdout
    mov rdx, 1 ; strlen
    syscall

    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число rdi в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10

    push rbx
    mov rbx, rsp

    dec rsp ; string-end
    mov byte[rsp], 0 ; null-terminator

    .loop:
        dec rsp
        xor rdx, rdx
        div r8
        add rdx, '0'
        mov byte[rsp], dl

        cmp rax, 0
        jg .loop

.ans:
    mov rdi, rsp
    call print_string

    mov rsp, rbx
    pop rbx
    ret

; Выводит знаковое 8-байтовое число rdi в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
    neg rdi
    mov rax, rdi
    mov rdi, '-'
    push rax
    call print_char
    pop rdi

    jmp print_uint

; Принимает два указателя на нуль-терминированные строки rdi rsi, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1
    mov rcx, 0
    .compare_char:
        mov bl, byte[rdi+rcx]
        mov dl, byte[rsi+rcx]
        cmp bl, dl
        jne .not_equal
    .check_null:
        cmp bl, 0
        je .end
        inc rcx
        jmp .compare_char

.not_equal:
    mov rax, 0

.end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ; sys_read
    xor rdi, rdi ; stdin
    dec rsp      ; string pointer
    mov rsi, rsp
    mov rdx, 1   ; string length
    syscall
    test rax, rax
    jz .EOF
    mov al, [rsp]
    jmp .END
    .EOF:
    xor rax, rax
    .END:
    inc rsp
    ret


; Принимает: адрес начала буфера rdi, размер буфера rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14

    mov r12, rdi ; buffer pointer
    mov r13, rsi ; buffer length
    dec r13      ; string length

    .skip:
        call read_char
        cmp al, 0
        je .fail
        cmp al, 0x20
        je .skip
        cmp al, 0xA
        je .skip
        cmp al, 0x9
        je .skip
        xor r14, r14
        jmp .word

    .word:
        mov byte [r12+r14], al
        inc r14
        cmp r14, r13
        jg .fail
        call read_char
        cmp al, 0
        je .ok
        cmp al, 0x20
        je .ok
        cmp al, 0xA
        je .ok
        cmp al, 0x9
        je .ok
        jmp .word

.fail:
    mov byte [r12], 0
    xor r12, r12
    xor r14, r14
    jmp exit

.ok:
    mov byte [r12+r14], 0
.exit:
    mov rdx, r14
    mov rax, r12

    pop r14
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10

.loop:
    mov r9, [rdi+rcx]
    and r9, 0xff
    cmp r9, '0'
    jl .exit
    cmp r9, '9'
    jg .exit
    
    mul r8
    add rax, r9
    sub rax, '0'
    inc rcx
    jmp .loop

.exit:
    mov rdx, rcx
    ret

; Принимает указатель на строку rdi, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push r12
    
    xor r12, r12
    cmp byte [rdi], '-'
    jne .parse_uint
    inc rdi
    mov r12, 1

.parse_uint:
    call parse_uint
    test r12, r12
    jz .exit
    inc rdx
    neg rax
.exit:

    pop r12
    ret 

; Принимает указатель на строку rdi, указатель на буфер rsi и длину буфера rdx
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    dec rdx
.loop:
    mov al, byte [rdi+rcx]
    mov byte [rsi+rcx], al
    cmp byte [rdi+rcx], 0
    je .exit
    inc rcx
    cmp rcx, rdx
    jg .error
    jmp .loop

.error:
    mov byte [rsi], 0
    xor rcx, rcx
    jmp .exit
.exit:
    mov rax, rcx
    ret

